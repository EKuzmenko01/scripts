#!/bin/sh
### Web server FreeBSD ver. 0.92 from 28.02.2011

### No need change in this script ###

os=`uname -a |grep -c FreeBSD`
if [ $os = "0" ]; then
echo "This script work only on FreeBSD. Exit."
exit
fi

echo "Enter username to create (default, django):"
read USER

echo "Enter username to create (default, django):"
read USERPASS

echo "Enter username to create (default, registrar):"
read NAMEPROJECT

if [ -z $USER ]; then
USER=django
fi

if [ -z $USERPASS ]; then
USERPASS=django
fi

if [ -z $NAMEPROJECT ]; then
NAMEPROJECT=registrar
fi

echo 'nginx_enable=yes' >> /etc/rc.conf
echo 'supervisord_enable=yes' >> /etc/rc.conf
echo 'postgresql_enable=YES' >> /etc/rc.conf
echo 'sshd_enable=YES' >> /etc/rc.conf

# check to exist homedir
#if [ ! -d /usr/home ]; then
#echo ./home is no exist! setup canceled..
#exit
#fi

# add additional dns servers, OpenDNS
echo .nameserver 8.8.8.8. >> /etc/resolv.conf

freebsd-update fetch
freebsd-update install

portsnap fetch
portsnap extract
portsnap update

make WITH_STATSD_MODULE=yes -DBATCH -C /usr/ports/www/nginx install clean
make -DBATCH -C /usr/ports/databases/postgresql94-server/ install clean
make -DBATCH -C /usr/ports/databases/postgresql94-contrib/ install clean
make -DBATCH -C /usr/ports/lang/python34/ install clean
make -DBATCH -C /usr/ports/devel/py-pip/ install clean
make -DBATCH -C /usr/ports/devel/py-virtualenv/ install clean
make -DBATCH -C /usr/ports/devel/git/  install clean
make -DBATCH -C /usr/ports/sysutils/py-supervisor install clean
pip install --upgrade pip

# Create Databases
/usr/local/etc/rc.d/postgresql initdb
service postgresql start
su - pgsql -c "createdb $NAMEPROJECT"

PASS="'"$USERPASS"'"
echo "create user $USER with password $PASS;" > 1
echo "GRANT ALL privileges ON DATABASE $NAMEPROJECT TO $USER;" >> 1

su - pgsql -c "psql -d $NAMEPROJECT -f /root/1"

# Create user and setup user folders
pw useradd $USER -d /home/$USER -m -s /bin/csh
echo "$USERPASS" | pw usermod $USER -h0

su - $USER -c "virtualenv --python=python3.4 myenv"
#su - $USER -c "cd /home/$USER/myenv/ && django-admin startproject $NAMEPROJECT"

su - django -c "source /home/$USER/myenv/bin/activate.csh && pip install Django==1.9.5 psycopg2==2.6.1 pytz==2016.3 gunicorn"
PROJ=https://bitbucket.org/elston/registrar-test/get/6004690fd057.zip 
su - $USER -c "cd /home/$USER/myenv && fetch $PROJ --no-verify-peer"
su - $USER -c "cd /home/$USER/myenv/ && unzip *.zip"
su - $USER -c "mv /home/$USER/myenv/elston* /home/$USER/myenv/$NAMEPROJECT"



mkdir /home/$USER/myenv/$NAMEPROJECT/logs
touch /home/$USER/myenv/$NAMEPROJECT/logs/django.log
chmod -R 777 /home/$USER/myenv/

echo ' ' >> /home/$USER/myenv/$NAMEPROJECT/$NAMEPROJECT/$NAMEPROJECT/settings.py
echo 'STATIC_ROOT = "/home/'$USER'/myenv/'$NAMEPROJECT'/static/"' >> /home/$USER/myenv/$NAMEPROJECT/$NAMEPROJECT/$NAMEPROJECT/settings.py
echo 'STATIC_URL = "/static/"' >> /home/$USER/myenv/$NAMEPROJECT/$NAMEPROJECT/$NAMEPROJECT/settings.py

su - $USER -c "source /home/$USER/myenv/bin/activate.csh && cd /home/$USER/myenv/registrar/registrar && ./manage.py makemigrations"
su - $USER -c "source /home/$USER/myenv/bin/activate.csh && cd /home/$USER/myenv/registrar/registrar && ./manage.py migrate"
su - $USER -c "source /home/$USER/myenv/bin/activate.csh && cd /home/$USER/myenv/registrar/registrar && ./manage.py collectstatic"

cd /home/$USER/myenv/bin
fetch http://5117287.ru/gunicorn_start
chmod u+x gunicorn_start


service supervisord start

cd /usr/local/etc

echo "[program:$NAMEPROJECT]" >> supervisord.conf
echo "command = sh /home/$USER/myenv/bin/gunicorn_start" >> supervisord.conf
echo "user = $USER" >> supervisord.conf
echo "stdout_logfile = /home/$USER/myenv/logs/gunicorn_supervisor.log" >> supervisord.conf
echo "redirect_stderr = true" >> supervisord.conf
echo "environment=LANG=en_US.UTF-8,LC_ALL=en_US.UTF-8" >> supervisord.conf

mkdir -p /home/$USER/myenv/logs/
touch /home/$USER/myenv/logs/gunicorn_supervisor.log
service supervisord restart

#configuiring nginx
cd /usr/local/etc/nginx/

mkdir -p vhost/
cd vhost/

fetch http://5117287.ru/mydjango.conf

cd /usr/local/etc/nginx/

fetch http://5117287.ru/nginx.conf

mkdir ssl
cd ssl

fetch http://5117287.ru/fullchain.pem
fetch http://5117287.ru/privkey.pem

service supervisord restart
service nginx start
